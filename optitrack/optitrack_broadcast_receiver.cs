using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using Unity.VisualScripting;
using UnityEngine;

public class OptitrackBroadcastReceiver : MonoBehaviour
{
	public class Marker
	{
		public double X, Y, Z;
	}

	public class Quaternion
	{
		public double Qx, Qy, Qz, Qw;
	}
	
	public class RigidBody
	{
		public Int32 ID;
		public Marker Center = new Marker();
		public Quaternion Rotation = new Quaternion();
	}
	
	public readonly List<Marker> NonRbMarkers = new List<Marker>();
	public readonly List<RigidBody> RigidBodies = new List<RigidBody>();
	
	
	private bool _receiving = false;
	private bool _newData = false;
	private UdpClient _udpClient;
	
	
	private void Callback(IAsyncResult ar)
	{
		const int si = sizeof(UInt32);
		const int ssi = sizeof(Int32);
		const int sd = sizeof(double);
		
		var remoteIpEndPoint = new IPEndPoint(IPAddress.Any, 0);
		var receiveBytes = _udpClient.EndReceive(ar, ref remoteIpEndPoint);

		var packetLen = BitConverter.ToUInt32(receiveBytes);
		var cur = 4;

		var nMarkers = BitConverter.ToUInt32(new ArraySegment<byte>(receiveBytes, cur, si));
		cur += si;

		NonRbMarkers.Clear();
		for (int i = 0; i < nMarkers; i++)
		{
			Marker m = new Marker();
			m.X = BitConverter.ToDouble(new ArraySegment<byte>(receiveBytes, cur, sd));
			cur += sd;
			m.Y = BitConverter.ToDouble(new ArraySegment<byte>(receiveBytes, cur, sd));
			cur += sd;
			m.Z = BitConverter.ToDouble(new ArraySegment<byte>(receiveBytes, cur, sd));
			cur += sd;
			
			NonRbMarkers.Add(m);
		}

		var nRigidBody = BitConverter.ToUInt32(new ArraySegment<byte>(receiveBytes, cur, si));
		cur += si;

		RigidBodies.Clear();
		for (int i = 0; i < nRigidBody; i++)
		{
			RigidBody rb = new RigidBody();

			rb.ID = BitConverter.ToInt32(new ArraySegment<byte>(receiveBytes, cur, ssi));
			cur += ssi;

			rb.Center.X = BitConverter.ToDouble(new ArraySegment<byte>(receiveBytes, cur, sd));
			cur += sd;
			rb.Center.Y = BitConverter.ToDouble(new ArraySegment<byte>(receiveBytes, cur, sd));
			cur += sd;
			rb.Center.Z = BitConverter.ToDouble(new ArraySegment<byte>(receiveBytes, cur, sd));
			cur += sd;
			
			rb.Rotation.Qx = BitConverter.ToDouble(new ArraySegment<byte>(receiveBytes, cur, sd));
			cur += sd;
			rb.Rotation.Qy = BitConverter.ToDouble(new ArraySegment<byte>(receiveBytes, cur, sd));
			cur += sd;
			rb.Rotation.Qz = BitConverter.ToDouble(new ArraySegment<byte>(receiveBytes, cur, sd));
			cur += sd;
			rb.Rotation.Qw = BitConverter.ToDouble(new ArraySegment<byte>(receiveBytes, cur, sd));
			cur += sd;
			
			RigidBodies.Add(rb);
		}
		
		_receiving = false;
		_newData = true;
	}


	// Start is called before the first frame update
	private void Start()
	{
		Debug.Log("optitrack_broadcast_receiver starting");
		// IPEndPoint localIPEndPoint = new IPEndPoint(IPAddress.Any, 15111);

		_udpClient = new UdpClient(15111);
		_udpClient.EnableBroadcast = true;
	}

	// Update is called once per frame
	private void Update()
	{
		if (!_receiving)
		{
			_receiving = true;
			_udpClient.BeginReceive(Callback, this);
		}

		if (_newData)
		{
			_newData = false;
			if (NonRbMarkers.Count > 0)
				Debug.Log(NonRbMarkers[0].X + " " +NonRbMarkers[0].Y + " " + NonRbMarkers[0].Z + " ");
		}
	}
}