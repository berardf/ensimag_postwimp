//
// Created by fberard on 09/11/23.
//

#ifndef OPTITRACK_PACKETCLIENT_H
#define OPTITRACK_PACKETCLIENT_H

#include "gil_optitrack.h"

std::string GetWSAErrorString( int errorValue );

extern int gNatNetVersion[4];
char* Unpack( char* pPacketIn );

class optitrack_packet_client_adapter_t
{ public:
	virtual				~optitrack_packet_client_adapter_t() = default;

	static void			unpack(char* packet) { ::Unpack(packet); }

	virtual void			add_rigid_body(int id, double x, double y, double z, double qx, double qy, double qz, double qw) = 0;

	virtual void			add_non_rb_marker(double x, double y, double z) = 0;
};

extern optitrack_packet_client_adapter_t* g_packet_client_adapter;




#endif //OPTITRACK_PACKETCLIENT_H
