//
// Created by François Bérard on 13/11/2023.
//

#include "gil_optitrack.h"
#include "gil_optitrack_packetclient.h"


#ifndef _WIN32
  #include <unistd.h>
  #include <sys/socket.h>
  #include <sys/types.h>
  #include <sys/socket.h>
  #include <arpa/inet.h>
  #include <netinet/in.h>
  #include <fcntl.h>

  #define GIL_CLOSE_SOCKET(x) close(x)

#else
  #include <winsock2.h>
  #include <tchar.h>
  #include <conio.h>
  #include <ws2tcpip.h>

  #define GIL_CLOSE_SOCKET(x) closesocket(x)
#endif


#ifndef _WIN32
static const int 		k_socket_protocol	= IPPROTO_UDP;
#else
static const int 		k_socket_protocol	= 0;
#endif


class adapter_t : public optitrack_packet_client_adapter_t
{ public:
	explicit 		adapter_t(gil::optitrack_t* optitrack) : _optitrack(optitrack), _g(optitrack->_gain) {}

				~adapter_t() override = default;
	
	void			add_non_rb_marker(double x, double y, double z) override
					{ _optitrack->_non_rb_markers.push_back({x*_g, y*_g, z*_g}); }
	void			add_rigid_body(int id, double x, double y, double z, double qx, double qy, double qz, double qw) override
					{ _optitrack->_rigid_bodies.push_back({(int32_t)id, {x*_g, y*_g, z*_g}, {qx, qy, qz, qw}}); }

	gil::optitrack_t*	_optitrack	{nullptr};
	double 			_g		{-1};
};

namespace gil {

void optitrack_t::init_optitrack_minimal()
{
	_rcv_buffer = (char*)malloc(k_rcv_buffer_size);
}

optitrack_t::~optitrack_t()
{
	if (_data_socket != -1)
		GIL_CLOSE_SOCKET(_data_socket);
	if (_broadcast_socket != -1)
		GIL_CLOSE_SOCKET(_broadcast_socket);
	
	if (_mcast_address != nullptr)
		free(_mcast_address);
	delete _rcv_buffer;
	delete _adapter;
	g_packet_client_adapter = nullptr;
}

bool optitrack_t::start(const int natnet_version[4], bool blocking, const std::string& local_interface, const std::string &multicast_group, int data_port)
{
	gNatNetVersion[0]	= natnet_version[0];
	gNatNetVersion[1]	= natnet_version[1];
	gNatNetVersion[2]	= natnet_version[2];
	gNatNetVersion[3]	= natnet_version[3];
	
	_multicast_group	= multicast_group;
	_data_port		= data_port;

	return this->open_socket(local_interface, _data_port, blocking);
}

bool optitrack_t::open_socket(const std::string&	local_interface,
			      int			port,
			      bool			blocking)
{
	struct sockaddr_in*	si_me;
	struct ip_mreq		mreq		{};
	int			res;
	int			option = 1;
#ifndef _WIN32
	in_addr_t		local_interface_bytes = htonl(INADDR_ANY);
	struct ip_mreq*		mreq_ptr	= &mreq;
#else
	ULONG			local_interface_bytes = inet_addr(local_interface.c_str());
	char*			mreq_ptr	= (char*)&mreq;
#endif

	if (_data_socket != -1) {
		fprintf(stderr, "optitrack_t::open_sockett: multicast socket already opened.\n");
		return false;
	}

	if ((_data_socket = (int)socket(AF_INET, SOCK_DGRAM, k_socket_protocol)) < 0) {
		fprintf(stderr, "optitrack_t::open_socket: could not create UDP socket.\n");
		goto failure;
	}

	if (setsockopt(_data_socket, SOL_SOCKET, SO_REUSEADDR, (const char*)&option, sizeof(option)) != 0) {
		fprintf(stderr, "optitrack_t::open_socket: could not set \"SO_REUSEADDR\" on socket.\n");
		goto failure;
	}

	// Save local address: we will use it to wake the listening thread by sending a packet.

	_mcast_address		= malloc(sizeof(struct sockaddr_in));
	si_me			= (struct sockaddr_in*)_mcast_address;
	memset((char*)si_me, 0, sizeof(*si_me));
	si_me->sin_family	= AF_INET;
	si_me->sin_port		= htons(port);
	si_me->sin_addr.s_addr	= local_interface_bytes;

	if (bind(_data_socket, (struct sockaddr*)si_me, sizeof(*si_me)) < 0) {
		fprintf(stderr, "optitrack_t::open_socket: could not bind UDP socket to local address and port:\nerror: \"%d\"\n", errno);
		goto failure;
	}

	mreq.imr_multiaddr.s_addr = inet_addr(_multicast_group.c_str());
	if (setsockopt(_data_socket, IPPROTO_IP, IP_ADD_MEMBERSHIP, mreq_ptr, sizeof(mreq)) < 0) {
		fprintf(stderr, "optitrack_t::open_socket: could not join multicast group (error: %d).\n", errno);
		goto failure;
	}

	if (!blocking) {
#ifndef _WIN32
		res = fcntl(_data_socket, F_SETFL, O_NONBLOCK);
#else
		u_long iMode = 1;
		res = ioctlsocket(_data_socket,FIONBIO,&iMode);
#endif
	}

	return true;

failure:
	if (_data_socket != -1) {
#ifndef _WIN32
		close(_data_socket);
#else
		closesocket(_data_socket);
#endif
		_data_socket = -1;
	}

	return false;
}

bool optitrack_t::start_broadcast()
{
	if ((_broadcast_socket = socket(AF_INET, SOCK_DGRAM, k_socket_protocol)) < 0) {
		fprintf(stderr, "optitrack_t::start_broadcast: could not create UDP broadcast socket.\n");
		return false;
	}

	int option = 1;
	if (setsockopt(_broadcast_socket, SOL_SOCKET, SO_BROADCAST, (const char*)&option, sizeof(option)) != 0) {
		fprintf(stderr, "optitrack_t::start_broadcast: could not set \"SO_BROADCAST\" on socket.\n");
		return false;
	}
	
	_broadcast_address	= malloc(sizeof(struct sockaddr_in));
	memset(_broadcast_address, 0, sizeof(struct sockaddr_in));

	struct sockaddr_in& s	= *((struct sockaddr_in*)_broadcast_address);
	s.sin_family		= AF_INET;
	s.sin_port		= htons(k_broadcast_port);
	s.sin_addr.s_addr	= htonl(INADDR_BROADCAST);

	return true;
}

bool optitrack_t::send_broadcast(void *message, size_t message_len)
{
	static bool 			log		= true;

#ifndef _WIN32
	#define packet message
#else
	const char* packet	= (const char*)message;
#endif

	int sent = sendto(_broadcast_socket, packet, int(message_len), 0, (struct sockaddr *)_broadcast_address, sizeof(struct sockaddr_in));
	if (sent < 0) {
 #ifndef _WIN32
		printf("optitrack_t::send_broadcast had error #%d\n", errno);
		return false;
#else
		int err = WSAGetLastError();
		printf("optitrack_t::send_broadcast had error #%d \"%s\"\n", err, GetWSAErrorString(err).c_str());
		fflush(stdout);
		return false;
#endif
	}
	if (log) {
		log = false;
		printf("re-Broadcasting received frames.\n");
		fflush(stdout);
	}
	return true;
}

bool optitrack_t::send_broadcast_frame()
{
	size_t 			n_marker	= _non_rb_markers.size();
	size_t 			n_rb		= _rigid_bodies.size();
	size_t 			packet_size	= 3 * sizeof(uint32_t) + n_marker * sizeof(position_t) + n_rb * sizeof(rigid_body_t);

	char*			packet		= (char*)malloc(packet_size);
	char*			cur		= packet;

	*((uint32_t*)cur)	= packet_size;
	cur			+= sizeof(uint32_t);

	*((uint32_t*)cur)	= n_marker;
	cur			+= sizeof(uint32_t);

	for (const auto& m: _non_rb_markers) {
		memcpy(cur, m._coord, 3 * sizeof(double));
		cur		+= 3 * sizeof(double);
	}

	*((uint32_t*)cur)	= n_rb;
	cur			+= sizeof(uint32_t);
	for (const auto& r: _rigid_bodies) {
		*((int32_t*)cur) = r._id;
		cur		+= sizeof(int32_t);
		memcpy(cur, r._center._coord, 3 * sizeof(double));
		cur		+= 3 * sizeof(double);
		memcpy(cur, r._rotation._coord, 4 * sizeof(double));
		cur		+= 4 * sizeof(double);
	}

	bool res = this->send_broadcast(packet, packet_size);

	free((void*)packet);

	return res;
}

bool optitrack_t::receive()
{
	int				nbytes;
	struct sockaddr_in		peer;
	socklen_t			slen		= sizeof(peer);
	static bool 			log		= true;

	if (_adapter == nullptr) {
		if (g_packet_client_adapter != nullptr)
			delete g_packet_client_adapter;
		g_packet_client_adapter = _adapter = new adapter_t(this);
	}
	
	if (_data_socket == -1) {
		printf("ERROR: optitrack_t::receive called but multicast socket is not openned.\n");
		return false;
	}

#ifndef _WIN32
	nbytes = recvfrom(_data_socket, (void*)_rcv_buffer, k_rcv_buffer_size, 0, (struct sockaddr*)&peer, &slen);
#else
	nbytes = recvfrom(_data_socket, (char*)_rcv_buffer, k_rcv_buffer_size, 0, (struct sockaddr*)&peer, &slen);
#endif

	if (nbytes == -1) {
#ifndef _WIN32
		if (errno != EAGAIN)
			printf("optitrack_t::receive with error #%d\n", errno);
		return false;
#else
		int err = WSAGetLastError();
		if (err != WSAEWOULDBLOCK)
			printf("optitrack_t::receive with error #%d \"%s\"\n", err, GetWSAErrorString(err).c_str());
		return false;
#endif
	}

	this->clear();
	optitrack_packet_client_adapter_t::unpack(_rcv_buffer);

	if (log) {
		log = false;
		printf("Received an optitrack frame with %d markers and %d rigid bodies.\n", int(_non_rb_markers.size()), int(_rigid_bodies.size()));
		fflush(stdout);
	}

	if (_broadcast_socket > 0)
		this->send_broadcast_frame();
	
	return false;
}



}
