# Sample code for receiving optitrack data frames

The project has 2 targets:

* optitrack_sc: a self-contained (sc) C file that receives optitrack frames and display them on stdout.
* gil_optitrack: a C++ program that uses a gil_optitrack class to receive optitrack frames. 

## Compilation

### On UNIX

```
mkdir build
cd build
cmake ..
make optitrack_sc
make gil_optitrack
```

### On all platforms (including Windows)

* Download [CLion](https://www.jetbrains.com/clion/download/)
* Launch CLion
* Open this "optitrack" directory


## Configuration

### In the Motive application on the optitrack PC

* Open the streaming pane (\<Edit\>\<Settings...\>\<Streaming\>)
* Check that "Local Interface" is set to the IP address of the ethernet card that is connected to the optitrack switch (i.e. *not* "loopback").
* Check that "Trasmission Type" is set to "Multicast"
* Enable streamin (switch at the top)

![Streaming pane in Motive](motive_streaming_pane.png)

### In "optitrack_sc.c"

* On Windows, set "k_local_interface" (at the beginning of "optitrack_minimal.c") to the IP address of the ethernet card that is connected to the optitrack switch *on the client PC* (i.e. not the optitrack PC). This is not used on UNIX.

* On Windows, when the firewall asks for authorization to connect, give authorizations. If admin priviledges are required, ask an admin.

* On Windows, the firewall may be disabled for "private" connections (define a private connection with set-netconnectionprofile -name “the name” -network private).


## gil_optitrack

* launch binary with the 'broadcast' parameter if you want it to resend the optitrack frames on a broadcast address.

```
gil_optitrack <local_interface> broadcast
```

* use the C# script 'optitrack_broadcast_receiver.cs' in Unity to receive these broadcasted frames.
