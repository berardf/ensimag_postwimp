//
// Created by François Bérard on 13/11/2023.
//

#ifndef GIL_OPTITRACK_MINIMAL_H
#define GIL_OPTITRACK_MINIMAL_H

#include <string>
#include <vector>

class adapter_t;

namespace gil {

class position_t
{ public:	position_t(double x, double y, double z) : _coord{x, y, z} {}
		double _coord[3] {0.0, 0.0, 0.0};
};

class quaternion_t
{ public:	quaternion_t(double x, double y, double z, double w) : _coord{x, y, z, w} {}
		double _coord[4] {0.0, 0.0, 0.0, 0.0};
};

class marker_vect_t : public std::vector<position_t> {};

class rigid_body_t
{ public:					rigid_body_t(int32_t id, const position_t& center, const quaternion_t& rotation):
							_id(id), _center(center), _rotation(rotation) {}
	int32_t 				_id			{-1};
	position_t				_center			{-1.0, -1.0, -1.0};
	quaternion_t				_rotation		{-1.0, -1.0, -1.0, -1.0};
};

class rigid_body_vect_t : public std::vector<rigid_body_t>
{ public:
	rigid_body_t*				get_from_id(int32_t id) { for (auto& rb : *this) if (rb._id == id) return &rb; return nullptr; }
};

class optitrack_t
{ public:

	static constexpr const char*	 	k_default_multicast_group	= "239.255.42.99";
	static constexpr const int 		k_default_command_port		= 1510;
	static constexpr const int 		k_default_data_port		= 1511;

	static constexpr const int 		k_broadcast_port		= 15111;

						optitrack_t()
							{ this->init_optitrack_minimal(); }

						~optitrack_t();

	bool					start	(const int 		natnet_version[4],
							 bool			blocking,
							 const std::string&	local_interface		= std::string(),
							 const std::string&	multicast_group		= std::string(k_default_multicast_group),
							 int			data_port		= k_default_data_port);

	bool 					start_broadcast();
	
	void					set_log_level(int log_level)	{ _log_level = log_level;	}
	int 					get_log_level() const		{ return _log_level;		}

	bool					receive();

	marker_vect_t&				get_non_rb_markers()		{ return _non_rb_markers;	}
	rigid_body_vect_t&			get_rigid_bodies()		{ return _rigid_bodies;		}
	
  protected:

	static constexpr const int 		k_rcv_buffer_size	= 20000;

	int 					_log_level		{1};	// 0 : no message, 1: errors only, 2: more informations
	int 					_data_socket		{-1};
	void*					_mcast_address		{nullptr};
	int 					_broadcast_socket	{-1};
	void*					_broadcast_address	{nullptr};
	std::string 				_multicast_group	{};
	int 					_data_port		{-1};
	char*					_rcv_buffer		{nullptr};

	double 					_gain			{1000.0}; // converts optitrack's meters to millimeters.
	marker_vect_t				_non_rb_markers		{};
	rigid_body_vect_t			_rigid_bodies		{};

	adapter_t*				_adapter		{nullptr};
	
	void					init_optitrack_minimal();
	void					clear()			{ _non_rb_markers.clear(); _rigid_bodies.clear(); }
	
	bool					open_socket (const std::string&		local_interface,
							     int			port,
							     bool			blocking);

	bool 					send_broadcast(void* message, size_t message_len);

	friend class ::adapter_t;

	bool send_broadcast_frame();
};


}

#endif //GIL_OPTITRACK_MINIMAL_H
