//
// Created by Francois Berard on 12/1/2023.
//

#include <cstdio>

#include "gil_optitrack.h"

#ifdef _WIN32
	#include <ws2tcpip.h>
#endif


static const int 		k_natnet_version[4]	= {4, 1, 0, 0};

void usage(const char* argv0)
{
	printf("Error: should be \"%s <local_interface_address> ?broadcast?\".\n", argv0);
}

int main(int argc, char* argv[])
{
	printf("gil_optitrack_test\n");

	if ((argc < 2) || (argc > 3)) {
		usage(argv[0]);
		return 1;
	}
	gil::optitrack_t		optitrack;

	printf("Attempting to start optitrack with local interface \"%s\" and multicast group \"%s\".\n", argv[1], gil::optitrack_t::k_default_multicast_group);
	fflush(stdout);

#ifdef _WIN32
	static bool	winsock_init	= false;
	WSADATA		WsaDat;
	if (!winsock_init) {
		winsock_init		= true;
		WSAStartup(MAKEWORD(2,2), &WsaDat);
	}
#endif

	if (!optitrack.start(k_natnet_version, true, argv[1])) {
		printf("ERROR: could not start optitrack.\n");
		return 1;
	}

	bool broadcast			{false};
	if (argc == 3) {
		if (strcmp(argv[2], "broadcast") != 0) {
			usage(argv[0]);
			return 1;
		}
		broadcast = true;
	}
	
	if (broadcast && !optitrack.start_broadcast())
		printf("WARNING: could not start broadcasting.\n");

	while(true)
		optitrack.receive();
	
	return 0;
}
